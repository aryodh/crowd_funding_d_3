from django.db import models
from register.models import Donasi_models
# Create your models here.
class campaign_item(models.Model):
	title = models.CharField(max_length = 300)
	target = models.DecimalField(max_digits=90, decimal_places=0)
	collected = models.DecimalField(max_digits=90, decimal_places=0)
	details = models.CharField(max_length = 10000)
	imageurl = models.CharField(max_length = 900, default="")
	donatur = models.ManyToManyField(Donasi_models, blank=True)

	def __str__(self):
		return self.title
