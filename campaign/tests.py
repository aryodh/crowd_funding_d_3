from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .views import item
from django.http import HttpRequest
from .models import campaign_item
from register.models import Donasi_models
class CampaignUnitTest(TestCase):
	def test_campaign_url_exists(self):
		response = Client().get('/campaign/')
		self.assertEqual(response.status_code, 200)
	def test_campaign_using_index_func(self):
		found = resolve('/campaign/')
		self.assertEqual(found.func, index)
	def test_item_using_item_func(self):
		#donaturs = Donasi_models.objects.all()
		new_camp = campaign_item.objects.create(title = 'a',target = 12,collected = 1,details = 'a',imageurl = 'a')
		link = '/campaign/item/' + str(new_camp.pk)+'/'
		found = resolve(link)
		self.assertEqual(found.func, item)
	def test_item_url_exist(self):
		new_camp = campaign_item.objects.create(title = 'a',target = 12,collected = 1,details = 'a',imageurl = 'a')
		link = '/campaign/item/' + str(new_camp.pk)+'/'
		response = Client().get(link)
		self.assertEqual(response.status_code, 200)
	def test_campaign_uses_campaign_as_template_(self):
		response = Client().get('/campaign/')
		self.assertTemplateUsed(response, 'campaign.html')
	def test_model_can_create_new_camp(self):
		new_camp = campaign_item.objects.create(title = 'a',target = 12,collected = 1,details = 'a',imageurl = 'a')
		counting_all_available_post = campaign_item.objects.all().count()
		self.assertEqual(counting_all_available_post, 1)
	def test_print_model(self):
		new_camp = campaign_item.objects.create(title = 'a',target = 12,collected = 1,details = 'a',imageurl = 'a')
		self.assertEqual(str(new_camp), new_camp.title)
		
	def test_item_offset_error_redirect(self):
		new_camp = campaign_item.objects.create(title = 'a',target = 12,collected = 1,details = 'a',imageurl = 'a')
		response = Client().get('/campaign/item/2/')
		self.assertEqual(response.status_code, 302)