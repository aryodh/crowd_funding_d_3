from django.urls import include, path
from django.conf.urls import url
from . import views

app_name = 'campaign'
urlpatterns = [
    url(r'^$', views.index, name = 'index'),
    url(r'item/(\d)/$', views.item, name = 'item'),
]