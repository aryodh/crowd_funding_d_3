from django.shortcuts import render
from django.http import Http404, HttpResponseRedirect
from .models import campaign_item
# Create your views here.
def index(request):
    response = {}
    response['location'] = 'Campaign'
    response['campaign_bar'] = 'active'
    response['campaign'] = campaign_item.objects.all()
    html = 'campaign.html'

    return render(request, html, response)
    
def item(request, offset):
    try:
        offset = int(offset)

        campaign_select = campaign_item.objects.get(pk=offset)
    except:
        return HttpResponseRedirect('/campaign')
    
    response = {}
    response['title'] = campaign_select.title
    response['details'] = campaign_select.details
    response['target'] = campaign_select.target
    response['collected'] = campaign_select.collected

    response['link_donate'] = campaign_select.pk
    response['percentage'] = int(response['collected']/response['target']*100)
    response['donatur'] = campaign_select.donatur.order_by('-pk')
    response['main_image'] = campaign_select.imageurl
    html = 'item.html'

    return render(request, html, response)