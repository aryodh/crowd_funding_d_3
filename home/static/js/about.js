
$(document).ready(function() {
    $('#commentForm').on('submit', function(e) {
        e.preventDefault();
        if ($('#commentArea').val() != "") {
			alert("Berhasil ditambahkan");
			$('#alert').removeClass("alert alert-danger");
			$('#alert').html("");
            $.ajax({
                url : 'add_testimoni/',
                type : "POST",
                data : {
                    user_name : $('#username').val(),
                    description : $('#commentArea').val(),
                    csrfmiddlewaretoken : $("input[name=csrfmiddlewaretoken]").val(),
                },
                success : function (response) {
                    var text = "<div class = 'container-fluid'><div class='card'><div class='card-header'>" + 
								response.user_name + "</div><div class='card-body'><div class='row'><div class='column'><img src='http://www.eurogeosurveys.org/wp-content/uploads/2014/02/default_profile_pic.jpg' height ='50px'alt=''></div><div class='column'><blockquote class='blockquote mb-0'><p>" 
								+ response.description + "</p><footer class='blockquote-footer'><small>On " + response.created_date + 
								"</small></footer></blockquote></div></div></div></div></div><br>"
					$('#commentArea').val("");
                        $("#isi").prepend(text);
                }
            });
        } else {
            $('#alert').addClass("alert alert-danger");
            $('#alert').html("Tolong isi kolom komentar");
        }
    });
})

