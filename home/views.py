from django.shortcuts import render
from django.http import HttpResponseRedirect
from campaign.models import campaign_item
from register.models import Donasi_models
from news.models import news_item

# Create your views here.
def index(request):
    response = {}
    response['location'] = 'Home'
    response['home_bar'] = 'active'
    response['campaign'] = campaign_item.objects.all()
    response['account'] = Donasi_models.objects.all()
    response['news'] = news_item.objects.all()[:3]
    response['donation'] = Donasi_models.objects.all().values('jumlah_donasi')
    response['total_campaign'] = len(response['campaign'])
    response['campaign'] = response['campaign'][:6]
    response['campaign_trend'] = response['campaign'][:2]
    response['total_account'] = len(response['account'])
    response['total_donation'] = sum([u['jumlah_donasi'] for u in response['donation']]) 

    html = 'index.html/'

    return render(request, html, response)
