from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .apps import HomeConfig

# Create your tests here.
class HomeUnitTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.url_name, 'index')

    def test_contain_SinarPerak(self):
        content = 'SinarPerak'
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(content, html_response)

    def test_apps(self):
        self.assertEqual(HomeConfig.name, 'home')
        self.assertEqual(apps.get_app_config('home').name, 'home')
