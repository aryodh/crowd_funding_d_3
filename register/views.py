from django.shortcuts import render
from django.http import Http404, HttpResponseRedirect, HttpResponse
from .models import Donasi_models
from .forms import Register_forms
from .forms import Donasi_forms
from campaign.models import campaign_item
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout as logoutUser
# Create your views here.


# Create your views here.
# def form(request):
# 	response = {}
# 	response["Register_forms"] = Register_forms
# 	return render(request, 'forms.html', response)
# Create your views here.
# def add_register(request):
# 	response = {}
# 	response['data'] = Register_forms
# 	response['failed'] = 0
# 	# response['daftarStatus'] = StatusHarian.objects.all().values()
# 	html = "forms.html"
# 	data_profile = Register_forms(request.POST)
# 	if request.method == 'POST' :
# 		response['nama_lengkap'] = request.POST['nama_lengkap']
# 		response['tanggal_lahir'] = request.POST['tanggal_lahir']
# 		response['email'] = request.POST['email']
# 		response['password'] = request.POST['password']

# 		if response['email'] not in [user['email'] for user in Register_models.objects.all().values('email')]:

# 			database = Register_models(
# 				nama_lengkap=response['nama_lengkap'], 
# 				tanggal_lahir=response['tanggal_lahir'], 
# 				email=response['email'], 
# 				password=response['password']
# 				)
# 			database.save()
# 			response['failed'] = 2
# 			#response['datatabase'] = Register_models.objects.all()
# 			return render (request, html, response)
# 		else:
# 			response['failed'] = 1
# 			return render(request, html, response)
# 	else:
# 		return render(request, html, response)

def add_donate(request, offset):
	if(request.user.is_authenticated != True):
		return HttpResponseRedirect('/register/login')
	
	try:
		offset = int(offset)
		campaign_select = campaign_item.objects.get(pk=offset)

	except:
		return HttpResponseRedirect('/campaign')

	response = {}
	response['campaign_tujuan'] = campaign_select
	response['donasi'] = Donasi_forms
	response['url'] = str(offset)
	html = "donasi_form.html"
	# response['daftarStatus'] = StatusHarian.objects.all().values()
	data_donasi = Donasi_forms(request.POST)
	if request.method == 'POST' :
		response['nama_orang'] = request.user.username
		response['email_donasi'] = request.user.email
		response['jumlah_donasi'] = request.POST['jumlah_donasi']
		response['pesan'] = request.POST['pesan']
		response['anonim'] = False
		if 'anonim' in request.POST:
			response['anonim'] = True

		campaign_select = campaign_item.objects.get(pk=offset)

		obj_donasi = Donasi_models(
			nama_orang=response['nama_orang'], 
			email_donasi=response['email_donasi'], 
			jumlah_donasi=response['jumlah_donasi'], 
			pesan=response['pesan'], 
			anonim=response['anonim'])
		obj_donasi.save()
		campaign_select.donatur.add(obj_donasi)
		campaign_select.collected = campaign_select.collected + int(response['jumlah_donasi'])
		campaign_select.save()
		link = '/campaign/item/' + str(offset)
		return HttpResponseRedirect(link)

	else:
		return render(request, html, response)

def login(request):
	if request.user.is_authenticated:
		request.session['user'] = request.user.username
		request.session['email'] = request.user.email
	return render(request, 'login.html')

def logout(request):
	request.session.flush()
	logoutUser(request)
	return HttpResponseRedirect('/register/login')
