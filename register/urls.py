from django.contrib import admin
from django.conf.urls import url, include
from django.contrib.auth import views
from .views import add_donate, login, logout


# Create your views here.
app_name = 'register'
urlpatterns = [
	url(r'add_donate/(\d)/$', add_donate, name = 'add_donate'),
	url(r'^login/$', login, name='login'),
    url(r'^logout/$', logout, name='logout'),
    url(r'^auth/', include('social_django.urls', namespace='social')),  # <- Here
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
]