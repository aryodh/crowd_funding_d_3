# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import add_donate, login, logout
from django.http import HttpResponse, HttpResponseRedirect
from .models import Donasi_models
from campaign.models import campaign_item
from django.contrib.auth.models import User


# Create your tests here.
class FormUnitTest(TestCase):

	def test_form_regist_exist(self):
		response = Client().get('/register/login')
		self.assertEqual(response.status_code,301)

	def test_form_using_login_func(self):
		found = resolve('/register/login/')
		self.assertEqual(found.func, login)

	def test_form_using_logout_func(self):
		found = resolve('/register/logout/')
		self.assertEqual(found.func, logout)

	def test_form_using_add_donate_func(self):
		nama='Nindya'
		jumlah = '10000'
		email_='nindyasavr@gmail.com'
		surat='sehat ya bu'
		anon = True 
		obj = campaign_item.objects.create(title='bencana', target=10, collected=10, details='bencana untuk donggala', imageurl='photo.jpg')
		found = resolve('/register/add_donate/' + str(obj.pk) + '/')
		self.assertEqual(found.func, add_donate)

	def test_models_donasi_can_save(self):
		new_register = Donasi_models.objects.create(nama_orang='Nindya', jumlah_donasi='10000', email_donasi='nindyasavr@gmail.com', pesan='Sehat ya bu', anonim=True)
		counting_objects_donasi = Donasi_models.objects.all().count()
		self.assertEqual(counting_objects_donasi, 1)

	def test_donasi_can_render(self):
		nama='Nindya'
		jumlah = '10000'
		email_='nindyasavr@gmail.com'
		surat='sehat ya bu'
		anon = True 
		obj = campaign_item.objects.create(title='bencana', target=10, collected=10, details='bencana untuk donggala', imageurl='photo.jpg')
		link = "/register/add_donate/" + str(obj.pk) + "/"
		response_post = Client().post(link, {'nama_orang': nama, 'jumlah_donasi' : jumlah, 'email_donasi' : email_, 'pesan': surat, 'anonim' : anon})
		self.assertEqual(response_post.status_code, 302)

	#def test_email_not_valid(self):
	#	new_register = Register_models.objects.create(nama_lengkap='Nindya', tanggal_lahir='1999-08-10', email='nindya@gmail', password='satu1234')
	#	new_donasi = Donasi_models.objects.create(nama_orang='Nindya', jumlah_donasi='10000', email_donasi='nindyasavr@gmail.com', pesan='Sehat ya bu', anonim=True)
	#	self.assertNotEqual(Register_models.objects.all().values('email'), Donasi_models.objects.all().values('email_donasi'))

	#def test_email_sudah_terdaftar(self):
	#	new_register = Register_models.objects.create(nama_lengkap='Nindya', tanggal_lahir='1999-08-10', email='nindyasavr@gmail.com', password='satu1234')
	#	nama='Nindya'
	#	tanggal_='1999-08-10' 
	#	email_='nindyasavr@gmail.com'
	#	password_='satu1234'
	#	response_post = Client().post('/register/', {'nama_lengkap': nama, 'tanggal_lahir' : tanggal_, 'email' : email_, 'password': password_})
	#	self.assertEqual(Register_models.objects.all().count(), 1)

	def test_donasi_link_not_found(self):
		response = Client().get('/register/add_donate/2/')
		self.assertEqual(response.status_code,302)

	def test_donasi_GET(self):
		obj = campaign_item.objects.create(title='bencana', target=10, collected=10, details='bencana untuk donggala', imageurl='photo.jpg')
		response = Client().get('/register/add_donate/1/')
		self.assertEqual(response.status_code, 302)		

	def test_models_donasi_can_save(self):
		new_register = Donasi_models.objects.create(nama_orang='Nindya', jumlah_donasi='10000', email_donasi='nindyasavr@gmail.com', pesan='Sehat ya bu', anonim=True)
		counting_objects_donasi = Donasi_models.objects.all().count()
		self.assertEqual(counting_objects_donasi, 1)

	def test_donasi_can_render(self):
		# user = request.user
		nama= 'Nindya'
		jumlah = '10000'
		email_= 'Nindya@gmail.com'
		surat='sehat ya bu'
		anon = True 
		obj = campaign_item.objects.create(title='bencana', target=10, collected=10, details='bencana untuk donggala', imageurl='photo.jpg')
		link = "/register/add_donate/" + str(obj.pk) + "/"
		response_post = Client().post(link, {'nama_orang': nama, 'jumlah_donasi' : jumlah, 'email_donasi' : email_, 'pesan': surat, 'anonim' : anon})
		self.assertEqual(response_post.status_code, 302)

	def test_offset_redirect(self):
		obj = campaign_item.objects.create(title='bencana', target=10, collected=10, details='bencana untuk donggala', imageurl='photo.jpg')
		response = Client().get('/campaign/item/2')
		self.assertEqual(response.status_code, 301)

	# def test_user_can_logout(self):
	# 	user = User(username = 'adindahawari')
	# 	user.set_password('hahahihihuhu')
	# 	user.save()
	# 	self.client.login(username = 'nindyasavr', password = 'hahahihihuhu')
	# 	response = self.client.get(reverse('register:login'))
	# 	self.assertJSONEqual(response.content, {'login': True})
	# 	self.client.get(reverse('register:logout'))
	# 	response = self.client.get(reverse('register:login'))
	# 	self.assertJSONEqual(response.content, {'login': False})

		