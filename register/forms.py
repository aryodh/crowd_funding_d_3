from django import forms

class Register_forms(forms.Form):
	nama_lengkap = forms.CharField(widget=forms.TextInput())
	tanggal_lahir = forms.DateField(widget=forms.DateInput(attrs={'type':'date'}))
	password = forms.CharField(widget=forms.PasswordInput())
	email = forms.EmailField()

class Donasi_forms(forms.Form):
	nama_orang = forms.CharField(widget=forms.TextInput())
	jumlah_donasi = forms.IntegerField(widget=forms.NumberInput())
	email_donasi = forms.EmailField()
	pesan = forms.CharField(widget=forms.TextInput())
	anonim = forms.BooleanField(required=False, initial=True)
	
