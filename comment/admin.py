from django.contrib import admin
from .models import testimoni

# Register your models here.
class TestimoniAdmin(admin.ModelAdmin):
    list_display = ["user_name", "description", "created_date"]
    list_filter = ["created_date"]
    search_fields = ["user_name", "description"]

    class Meta:
        model = testimoni
admin.site.register(testimoni, TestimoniAdmin)