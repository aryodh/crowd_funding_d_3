from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index,item
from .models import news_item


# Create your tests here.
class newsViewsTemplateTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code,200)

    def test_url_using_index_func(self):
    	found = resolve('/news/')
    	self.assertEqual(found.func, index)

    def test_using_update_news_template(self):
        response = Client().get('/news/')
        self.assertTemplateUsed(response,'news.html')

    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('BMKG: Gempa Susulan Masih Terjadi di Palu', html_response)

    def test_details(self):
        newss = news_item.objects.create(title="title", imageurl="test", details="test")
        link = '/news/item/' + str(newss.pk)+'/'      
        response = Client().get(link)
        self.assertEqual(response.status_code, 200)

    def test_item_offset_error_redirect(self):
        newss = news_item.objects.create(title="title", imageurl="test", details="test")
        response = Client().get('/news/item/7/')
        self.assertEqual(response.status_code, 302)

# Create your tests here.
class newsModelTest(TestCase):
    def test_if_instance_is_created(self):
        news_item.objects.create(title="title",
                            imageurl="test",
                            details="test")
        hitung_count = news_item.objects.count()
        self.assertEqual(hitung_count, 1)
