from django.db import models

# Create your models here.
class news_item(models.Model):
	title = models.CharField(max_length = 300)
	details = models.TextField()
	imageurl = models.CharField(max_length = 900, default="")
