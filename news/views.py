from django.shortcuts import render, HttpResponse
from django.http import Http404, HttpResponseRedirect
from .models import news_item

# Create your views here.
def index(request):
    news = news_item.objects.all()
    return render(request, 'news.html', {"news" : news})

def item(request, offset):
    try:
        news_select = news_item.objects.get(pk=offset)
        response = {}
        response['title'] = news_select.title
        response['details'] = news_select.details
        response['photo'] = news_select.imageurl
        html = 'news_item.html'
        return render(request, html, response)
    except:
        return HttpResponseRedirect('/news')
    