# crowd_funding_d_3

## Nama anggota kelompok:
 - Aryo Tinulardhi - (1706039515) 
 - Clarisa Mirah Sekar Sari - (1706984530)
 - Hana Raissya - (1706979266) 
 - Nindya Savirahandayani - (1706074745)
    

## Status
[![pipeline status](https://gitlab.com/aryodh/crowd_funding_d_3/badges/master/pipeline.svg)](https://gitlab.com/aryodh/crowd_funding_d_3/commits/master)
[![coverage report](https://gitlab.com/aryodh/crowd_funding_d_3/badges/master/coverage.svg)](https://gitlab.com/aryodh/crowd_funding_d_3/commits/master)

## Link Heroku App
[https://crowd-funding-d-3.herokuapp.com/]

## Link Git Repository
[https://gitlab.com/aryodh/crowd_funding_d_3.git]
