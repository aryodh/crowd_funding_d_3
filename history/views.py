from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect
from register.models import Donasi_models
from campaign.models import campaign_item

# Create your views here.
def index(request):
    if request.user.is_authenticated:
        html = "history.html"
        print("masuk")
        response = {"location" : "History"}
        return render(request, html, response)
    else:
        return HttpResponseRedirect("/register/login")


def history_webservice(request):
    if request.user.is_authenticated:
        user_email = request.user.email
        user_donations = list(Donasi_models.objects.filter(email_donasi = user_email))
        list_donation = [{"imageurl" : i.campaign_item_set.all()[0].imageurl, "jumlah_donasi" : i.jumlah_donasi, "title" : i.campaign_item_set.all()[0].title} for i in user_donations]
        
        return JsonResponse([request.user.username, list_donation], safe = False)
    else:
        return HttpResponseRedirect("/")