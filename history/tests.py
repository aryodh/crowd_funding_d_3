from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .apps import HistoryConfig
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model


# Create your tests here.

class HistoryUnitTest(TestCase):
    
    def test_url_is_exist(self):
        cli = Client()
        response = cli.get("/history/")
        self.assertEqual(response.status_code, 302)


    def test_tp2_using_index_func(self):
        found = resolve('/history/')
        self.assertEqual(found.url_name, 'index')

    def test_apps(self):
        self.assertEqual(HistoryConfig.name, 'history')
        self.assertEqual(apps.get_app_config('history').name, 'history')
        

class TestAuthenticateUser(TestCase):
    def setUp(self):
        User = get_user_model()
        user = User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')

    def test_secure_page(self):
        User = get_user_model()
        self.client.login(username='temporary', password='temporary')
        response = self.client.get('/history', follow=True)
        self.assertEqual(response.status_code, 200)
        
    def test_web_service_fail(self):
        response = Client().get("/history/user_email")
        self.assertEqual(response.status_code, 302)
    
    def test_web_service_success(self):
        User = get_user_model()
        self.client.login(username='temporary', password='temporary')
        response = self.client.get('/history/user_email', follow=True)
        self.assertEqual(response.status_code, 200)

