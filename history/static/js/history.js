$(document).ready(function() {
    web_service();
});

function web_service() {
    $.ajax({
        url:"/history/user_email",
        success: function(data) {
            var result = "";
            var total = 0;
            console.log(data);
            var name = data[0];
            data = data[1];
            for (var i = 0; i < data.length; i++) {
                total += data[i].jumlah_donasi;

                result += '<div style="background: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url(' + 
                data[i].imageurl + ');" class="card">' + 
                '<div class="card-body">' +
                data[i].title + '<hr>' +
                '<b>Rp.' + data[i].jumlah_donasi + '</b>' +
                '</div>' +
                '</div>'

            }
            $("#history-content").append(result);
            $("#total-campaign").append(data.length);
            $("#total-donation").append(total);
            $("#username").append(name);
        }
    });
};