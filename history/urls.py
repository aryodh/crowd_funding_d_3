from django.contrib import admin
from django.urls import path
from . import views

app_name = "history"
urlpatterns = [
    path('index', views.index, name = "index"),
    path('user_email', views.history_webservice, name = "history"),
    path('', views.index, name = "index"),
]